import Vue from 'vue'
import Router from 'vue-router'
import Film from './components/Film/index.vue'
import List from './views/List.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'list',
      component: List
    },
    {
      path: '/:id',
      name: 'film',
      component: Film
    }
  ]
})
